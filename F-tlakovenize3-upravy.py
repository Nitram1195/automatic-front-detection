#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#knihovny
from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap, cm
from scipy.optimize import leastsq

#Pomocné moduly
import computation as cp
import frontOutput
import netCDFfunctions 

np.set_printoptions(threshold=np.nan)

def main():
    #Načtení dat
    file_T = Dataset("icon_global_icosahedral_pressure-level_2018121500_000_850_T.grib2_1x1.nc","r")
    file_V = Dataset("icon_global_icosahedral_pressure-level_2018121500_000_850_V.grib2_1x1.nc","r")
    file_U = Dataset("icon_global_icosahedral_pressure-level_2018121500_000_850_U.grib2_1x1.nc","r")
    file_pressure = Dataset("icon_global_icosahedral_single-level_2018121500_000_PMSL.grib2_1x1.nc","r") 
    
    tlakove_pole = file_T.variables["lev"][:]
    
    hladina_tlaku = 0
    
    teplota = file_T['t'][0,hladina_tlaku,] #bereme v 850 hp
    lats = file_T["lat"][:]  # (-90,90)   theta
    lons = file_T["lon"][:]  # (-180,180) s
    U = file_U['u'][0,hladina_tlaku,]
    V = file_V['v'][0,hladina_tlaku,]
    p  = file_pressure["prmsl"][0]
    
    
    lats = np.pi/2.0 - lats/180.0*np.pi # polar angle = theta (0,pi)
    lons = np.pi + lons/180.0*np.pi # azimuth = lambda (-pi,pi)
    konstanta=0.0000045 #K/m
    
    #rozšíření planyty za hrnice pro zlepšení detekce celoplanetárně
    U2 = np.tile(U, (1,3))
    V2 = np.tile(V, (1,3))
    teplota2 = np.tile(teplota, (1,3))
    extra_part_length = int(len(lons) / 4)
    lons2 = np.tile(lons, 30)
    
    U = U2[:,extra_part_length*3:extra_part_length*9]
    V = V2[:,extra_part_length*3:extra_part_length*9]
    teplota = teplota2[:,extra_part_length*3:extra_part_length*9]
    lons = lons2[extra_part_length*3:extra_part_length*9]
    
    #Vyhlazení proměnných - zatim jsem pouzival pouze 2 iterace z časových duvodu ladeni kodu
    pocet_iteraci = 2
    teplota = cp.smooth(teplota, pocet_iteraci)
    U=cp.smooth(U, pocet_iteraci)
    V=cp.smooth(V, pocet_iteraci)
    p=cp.smooth(p, 30)
    
    minimaTlakoveNize = cp.najdi_tlakove_nize(p)       #pole tlakovych nizi          
    
    
    #vypočet frontálního parametru
    gradient = cp.spherical_gradient(teplota,lats,lons,norm=True)
    vorticita = cp.vorticity(U,V,lats,lons)
    parametr =  vorticita*gradient/(konstanta*cp.coriolis_parameter(lats,lons))
    
    number_of_lats = np.prod(parametr.shape[0]) #? proc prod ? 
    number_of_lons = np.prod(parametr.shape[1]) #? proc prod ?
            
    #vyhlazeni na rovniku a polech
    uprava_poly_rovnik(parametr)
    """ 
    #výstupní hodnoty parametru
    parametr_print = parametr[:,extra_part_length:extra_part_length*5]
    
    #zapis do souboru
    netCDFfunctions.write_data("F-diagnostic.nc",parametr_print,file_T["lat"][:],file_T["lon"][:])
    """       
    #vybereme frontalni oblasti
    vyber_frontalni_oblasti(parametr,tlakove_pole[hladina_tlaku]);        
    
    #odebrani malych oblasti
    odeber_male_oblasti(parametr,15)
    
    sortedarray= uloz_fronty(parametr)
    
    
    #výstupní hodnoty parametru
    parametr_print = parametr[:,extra_part_length:extra_part_length*5]
    
    #vykreslení na mapu
    fig = plt.figure(figsize = (8,8))
    ax = fig.add_axes([0.1,0.1,0.8,0.8]) #pouziti?
    
    m = Basemap(projection = "cyl", lat_0 = 0, lon_0 = 0, resolution = "l")
    
    m.drawcoastlines()
    m.drawcountries()
    
    parallels = np.arange(-90., 91., 30.)
    meridians = np.arange(-180., 181., 45.)
    m.drawparallels(parallels, labels = [True, False, False, False])
    m.drawmeridians(meridians, labels = [True, False, False, True])
    
    ny = parametr_print.shape[0]
    nx = parametr_print.shape[1]
    
    gridlons, gridlats = m.makegrid(nx, ny)
    x,y = m(gridlons, gridlats)
    
    #obsah_ploch(parametr,1)
    
    tepl_skala = np.arange(245, 325, 2) #pouziti?
    skala = np.arange(- 1, 1.005, 0.05)
    cs = m.contourf(x, y, parametr_print, skala, cmap = plt.cm.coolwarm)
    cbar = m.colorbar(cs, location = "bottom", pad = "10%") #pouziti?
        
    fronts, frontssmer, frontal_points, frontstyp = fit(sortedarray,parametr,x,y,m,teplota)

    #plot a uložení do souboru
    #plt.show()
    
    #nalezeni lokalnich tlakovych minim 
                            
    #funkce ktera priradi promenne lokalni minimum a vykresli
    
    def najdimin():
        indexminimaTlakoveNize=0; 
        for fronta in frontal_points:
            vzdalenost =0
            for minimum in range (len(minimaTlakoveNize)):
                tmpvzdalenost = 0
                for prvek in range(len(frontal_points[fronta][0])):
                    xn = y[minimaTlakoveNize[minimum][0]-1,0]                                                                    #tady prevadim indexy na lat/lon aby to šlo vykreslit
                    yn = x[0,minimaTlakoveNize[minimum][1]-1]
                    tmpvzdalenost = tmpvzdalenost + (frontal_points[fronta][1][prvek]-yn)**2+(frontal_points[fronta][0][prvek]-xn)**2
                if (tmpvzdalenost < vzdalenost or vzdalenost ==0) :
                    vzdalenost = tmpvzdalenost;
                    indexminimaTlakoveNize=minimum
            #  np.append(frontal_points[fronta][0],minimaTlakoveNize[indexminimaTlakoveNize][0])
            # frontal_points[fronta]=np.append(frontal_points[fronta],[[minimaTlakoveNize[indexminimaTlakoveNize][0]],[minimaTlakoveNize[indexminimaTlakoveNize][1]]],axis=1)
            xn = y[minimaTlakoveNize[indexminimaTlakoveNize][0]-1,0]                                                                    #tady prevadim indexy na lat/lon aby to šlo vykreslit
            yn = x[0,minimaTlakoveNize[indexminimaTlakoveNize][1]-1]
            for j in range(5):
                frontal_points[fronta]=np.append(frontal_points[fronta],[[xn],[yn]],axis=1)
            m.plot(yn, xn,'ko', color = "pink", markersize=8)              
    
    #vykresleni vsech lokalnich minim
    def vykresli_tlakove_nize():
        barva = "green"
        for minimum in range (len(minimaTlakoveNize)):
            xn = y[minimaTlakoveNize[minimum][0]-1,0]    #tady prevadim indexy na lat/lon aby to šlo vykreslit
            yn = x[0,minimaTlakoveNize[minimum][1]-1]
            m.plot(yn, xn,'ko', color = barva, markersize=4)
    
    #vykresli_tlakove_nize()
    #najdimin()
    print(frontal_points)
    najdi_tlak_min(frontal_points,p,m,y,x)
    
    
    frontOutput.vypis(fronts, frontssmer,frontstyp)
    
    
    fig.savefig("F-diagnostic_picture.png", dpi=640)

def uprava_poly_rovnik(parametr):
        number_of_lats = parametr.shape[0] #? proc prod ? 
        number_of_lons = parametr.shape[1] #? proc prod ?
        #zhlazení kolem rovníku
        parametr[int(75*number_of_lats/180):int(105*number_of_lats/180), :] = 0
    
        for j in range(0, number_of_lons):
            for i in range(int(70 * number_of_lats/180), int(75 * number_of_lats/180)):
                parametr[i, j] = parametr[i, j] * (-1/(75 * number_of_lats/180 - 70 * number_of_lats/180) * i + (75 * number_of_lats/180)/(75 * number_of_lats/180 - 70 * number_of_lats/180))
    
        for j in range(0, number_of_lons):
            for i in range(int(105 * number_of_lats/180), int(110 * number_of_lats/180)):
                parametr[i, j] = parametr[i, j] * (+1/(110 * number_of_lats/180 - 105 * number_of_lats/180) * i - (105 * number_of_lats/180)/(110 * number_of_lats/180 - 105 * number_of_lats/180))
        
        #zhlazení na pólech
        parametr[0: int(25 * number_of_lats/180), :] = 0
        parametr[int(160 * number_of_lats/180):int(180 * number_of_lats/180), :] = 0
        
        for j in range(0, number_of_lons):
            for i in range(int(25 * number_of_lats/180), int(30 * number_of_lats/180)):
                parametr[i, j] = parametr[i, j] * (+1/(30 * number_of_lats/180 - 25 * number_of_lats/180) * i - (25 * number_of_lats/180)/(30 * number_of_lats/180 - 25 * number_of_lats/180))
        
        for j in range(0, number_of_lons):
            for i in range(int(155 * number_of_lats/180), int(160 * number_of_lats/180)):
                parametr[i, j] = parametr[i, j] * (-1/(160 * number_of_lats/180 - 155 * number_of_lats/180) * i + (160 * number_of_lats/180)/(160 * number_of_lats/180 - 155 * number_of_lats/180))

       
def vyber_frontalni_oblasti(parametr,hladina_tlaku):
        number_of_lats = parametr.shape[0] 
        number_of_lons = parametr.shape[1] #? proc prod ?
        #izobarická hladina a nastavení kritické hodnoty parametru
        hpa = hladina_tlaku/100
        kriticka_hodnota = hpa/-300. + 4.
    
        for j in range(0, number_of_lons):
            for i in range(0, number_of_lats):
                if parametr[i, j] > kriticka_hodnota:
                    parametr[i, j] = 1
                elif parametr[i, j] < - kriticka_hodnota:
                    parametr[i, j] = - 1
                else:
                    parametr[i, j] = 0
                    
def prozkoumej_sousedy(parametr,x,y,indexy,obsah):
            if x != parametr.shape[0]-1 and parametr[x+1,y]==1:
                obsah[0] += 1
                indexy.append([x+1,y])
                parametr[x+1,y]=0
                prozkoumej_sousedy(parametr,x+1,y,indexy,obsah)
            if y != 0 and parametr[x,y-1]==1:
                obsah[0] += 1
                indexy.append([x,y-1])
                parametr[x,y-1]=0
                prozkoumej_sousedy(parametr,x,y-1,indexy, obsah)
            if y != parametr.shape[1]-1 and parametr[x,y+1]==1:
                obsah[0] += 1;
                indexy.append([x,y+1])
                parametr[x,y+1]=0
                prozkoumej_sousedy(parametr,x,y+1,indexy,obsah)
            if x!= 0 and parametr[x-1,y]==1:
                obsah[0] += 1;
                indexy.append([x-1,y])
                parametr[x-1,y]=0
                prozkoumej_sousedy(parametr,x-1,y,indexy,obsah)
    
def prozkoumej_sousedyminus(parametr,x,y,indexy,obsah):
            if x != parametr.shape[0]-1 and parametr[x+1,y]==-1:
                obsah[0] += 1
                indexy.append([x+1,y])
                parametr[x+1,y]=0
                prozkoumej_sousedyminus(parametr,x+1,y,indexy,obsah)
            if y != 0 and parametr[x,y-1]==-1:
                obsah[0] += 1
                indexy.append([x,y-1])
                parametr[x,y-1]=0
                prozkoumej_sousedyminus(parametr,x,y-1,indexy,obsah)
            if y != parametr.shape[1]-1 and parametr[x,y+1]==-1:
                obsah[0] += 1;
                indexy.append([x,y+1])
                parametr[x,y+1]=0
                prozkoumej_sousedyminus(parametr,x,y+1,indexy,obsah)
            if x!= 0 and parametr[x-1,y]==-1:
                obsah[0] += 1;
                indexy.append([x-1,y])
                parametr[x-1,y]=0
                prozkoumej_sousedyminus(parametr,x-1,y,indexy,obsah)

def odeber_male_oblasti(parametr,prah):
        for i in range(parametr.shape[0]):
                for j in range (parametr.shape[1]):
                    obsah = [0] #používame list, protoze je mutable (jako bychom predavali pomoci reference)
                    indexy = []
                    if parametr[i,j] == 0: continue
                    elif parametr[i,j] == 1:
                        obsah[0]=1
                        indexy.append([i,j])
                        parametr [i,j]=0
                        prozkoumej_sousedy(parametr,i,j,indexy,obsah)
                        #print(obsah)
                        if obsah[0] > prah:
                            for index in indexy:
                                parametr[index[0],index[1]]=1  
                    elif parametr[i,j] == -1:
                        obsah[0]=1
                        indexy.append([i,j])
                        parametr [i,j]=0
                        prozkoumej_sousedyminus(parametr,i,j,indexy,obsah)
                        #print(obsah)
                        if obsah[0] > prah:
                            for index in indexy:
                                parametr[index[0],index[1]]=-1
                                
def uloz_fronty(parametr):
        number_of_lats = parametr.shape[0] 
        number_of_lons = parametr.shape[1] #? proc prod ?

        #záznam front a fitování křivek
        longarray = np.array([[0, 0]]) # [i, 0] je lattitude, [i, 1] je longitude
        
        #výběr všech frontálních bodů
        for i in range(0, number_of_lats):
        
            for j in range(0, number_of_lons):
        
                if parametr[i,j] != 0:
        
                    longarray = np.vstack((longarray, [i,j]))
        
        sortedarray = np.array([[0,0,0]]) # první číslo je ID fronty, zbytek je longarray
        
        #přičlenění frontálních bodů ke svým frontám (z longarray postupné mazání a přesun do sortedarray)
        number_of_fronts = 0
        
        front_lengths = []
        
        while longarray.shape[0] > 1:
        
            number_of_fronts += 1
        
            sortedarray = np.vstack((sortedarray, [number_of_fronts, longarray[1,0], longarray[1,1]]))
        
            longarray = np.delete(longarray, 1, axis = 0)
        
            belongs_to_same_front = True # hlídání přiřazení ke stejné frontě
        
            skip_previous_fronts = sortedarray.shape[0] - 1 # velké zrychlení algoritmu
        
            temperature_of_the_front = parametr[sortedarray[skip_previous_fronts, 1], sortedarray[skip_previous_fronts, 2]] # zlepšení rozlišování jednotlivých front
        
            counter = 1
        
            while belongs_to_same_front:
        
                belongs_to_same_front = False
        
                for i in range(skip_previous_fronts, sortedarray.shape[0]):
        
                    for j in range(0, longarray.shape[0]):
        
                        if j >= longarray.shape[0]:
        
                            break
        
                        if np.abs(sortedarray[i, 1] - longarray[j, 0]) <= 3 and np.abs(sortedarray[i, 2] - longarray[j, 1]) <= 3 and temperature_of_the_front == parametr[longarray[j, 0], longarray[j,1]]:
        
                            sortedarray = np.vstack((sortedarray, [number_of_fronts, longarray[j,0], longarray[j,1]]))
        
                            longarray = np.delete(longarray, j, axis = 0)
        
                            belongs_to_same_front = True
        
                            counter += 1
        
            front_lengths.append(counter)
        
        sortedarray = np.delete(sortedarray, 0, axis = 0) #smazání iniciační hodnoty pole
        
        index_of_duplicates = []
        
        for i in range(0, len(sortedarray)):
            for j in range(i+1, len(sortedarray)):
        
                if (sortedarray[i][1] == sortedarray[j][1] and sortedarray[i][2] + 360 == sortedarray[j][2]):
        
                    if front_lengths[sortedarray[i][0] - 1] < front_lengths[sortedarray[j][0] - 1]:
        
                        index_of_duplicates.append(i)
        
                    else:
        
                        index_of_duplicates.append(j)
        
        sortedarray = np.delete(sortedarray, index_of_duplicates, 0)
        
        fix_front_value = 0
        count = 1
        
        for i in range(0, len(sortedarray)):
        
            if (sortedarray[i][0] == fix_front_value):
        
                sortedarray[i][0] = count
        
            else:
        
                if sortedarray[i][0] != count:
        
                    fix_front_value = sortedarray[i][0]
        
                    count += 1
        
                    sortedarray[i][0] = count
        return sortedarray;


def fit(sortedarray,parametr,x,y,m,teplota):
    
    #hranice určení fronty
    Rsquared_threshold_linear = 50e-2 #max 70
    Rsquared_threshold_curved = 1e-2 #mezi 1 a 10
        
    fronts = {}
    frontssmer = []
    frontstyp = []
    number_of_fronts = 1
    frontal_points = {}
    number_of_frontal_point_areas = 1
    
    Front_min_length = 8 #minimální délka fronty
    for ii in range (1, sortedarray[len(sortedarray)-1][0]+1):
        hodnotyx=[]
        hodnotyy =[]
        for pole in sortedarray:
            if pole[0]==ii:
                hodnotyx.append(pole[1])
                hodnotyy.append(pole[2])
        
        hodnotyx = np.asarray(hodnotyx) #bude to np.array
        hodnotyy = np.asarray(hodnotyy)
    
        merge_hodnoty = np.vstack((hodnotyx, hodnotyy))
    
        for l in range(0, len(hodnotyy)):
            merge_hodnoty[1,l] = merge_hodnoty[1,l] - 90
    
            if merge_hodnoty[1,l] >= 360:
    
                merge_hodnoty[1,l] = merge_hodnoty[1,l] - 360
    
            if merge_hodnoty[1,l] < 0:
    
                merge_hodnoty[1,l] = merge_hodnoty[1,l] + 360
        
        merge_hodnoty[0] = y[merge_hodnoty[0], 0]
        merge_hodnoty[1] = x[0, merge_hodnoty[1]]
    
        frontal_points.update({"front"+str(number_of_frontal_point_areas): merge_hodnoty})
        number_of_frontal_point_areas += 1
    
        params = [1, 1, 1, 1]  #nastrel hodnot a.b.c.alfa
    
        narrow_fit, _ = leastsq(functionF, params, args = (hodnotyx, hodnotyy), maxfev = 2000 * len(hodnotyx))
    
        a = narrow_fit[0]
        b = narrow_fit[1]
        c = narrow_fit[2]
        alpha = narrow_fit[3]
    
        x_turned = (np.cos(alpha) * hodnotyx + np.sin(alpha) * hodnotyy)
        y_turned = (-np.sin(alpha) * hodnotyx + np.cos(alpha) * hodnotyy)
    
        y_calculated = a * np.power(x_turned, 2) + b * x_turned + c 
    
        residual = np.sum(np.power(y_turned - y_calculated,2))
        ymean = np.mean(y_turned)
        sumtot = np.sum(np.power(y_turned - ymean, 2))
        Rsquared = 1 - (residual/sumtot)
     
        if Rsquared > Rsquared_threshold_curved:
    
            minim = int(np.floor(min(x_turned)))
            maxim = int(np.ceil(max(x_turned)))
    
            xx_new = np.array([xx for xx in range(minim,maxim)])
    
            if (len(xx_new) > Front_min_length):
            
                yy_new = a * np.power(xx_new, 2) + b * xx_new + c
    
                x_fin = (np.cos(alpha) * xx_new - np.sin(alpha) * yy_new)
                y_fin = (np.cos(alpha) * yy_new + np.sin(alpha) * xx_new)
    
                xn = (x_fin).astype(int)
                yn = (y_fin).astype(int)
    
                for l in range(0, len(yn)):
                    yn[l] = yn[l] - 90
    
                    if yn[l] >= 360:
    
                        yn[l] = yn[l] - 360
    
                    if yn[l] < 0:
    
                        yn[l] = yn[l] + 360
                
                xn = y[xn,0]                 #tady prevadim indexy na lat/lon aby to šlo vykreslit
                yn = x[0,yn]
    
                for i in range(0, len(yn)):
                    if yn[i] > 180:
                        yn[i] = 180

                    if yn[i] < -180:
                        yn[i] = -180

                crosspoint = 0

                combo = np.vstack((xn, yn))
    
                fronts.update({"front"+str(number_of_fronts): combo})
                number_of_fronts += 1
    
                typ_fronty = int(parametr[hodnotyx[1], hodnotyy[1]])
    
                smer_fronty = smer_front(typ_fronty,hodnotyx,hodnotyy,teplota,xn,yn)
                frontssmer.append(smer_fronty)
                frontstyp.append(typ_fronty)
    
                #print(typ_fronty, ":", smer_fronty)

                #vykreslení na mapu
                

                for l in range(0, len(yn)):
                    if yn[l] == -180. or yn[l] == 180.:
                        crosspoint = l
    
                barva = "green"
    
                if (parametr[hodnotyx[0], hodnotyy[0]] == 1):
    
                    barva = "yellow"
    
                elif (parametr[hodnotyx[0], hodnotyy[0]] == -1):
    
                    barva = "purple"
    
                if crosspoint != 0:
    
                    yn1 = yn[:crosspoint]
                    xn1 = xn[:crosspoint]
                    yn2 = yn[crosspoint:]
                    xn2 = xn[crosspoint:]
    
                    m.plot(yn1, xn1, color = barva)
                    m.plot(yn2, xn2, color = barva)
    
                else:
    
                    m.plot(yn, xn, color = barva)
    
    #dodatečný fit přímkou
        else:
            first_fit = np.polyfit(hodnotyx, hodnotyy, 1)
    
            y_calculated = np.polyval(first_fit, hodnotyx)
            residual = np.sum((hodnotyy - y_calculated)**2)
            ymean = np.mean(hodnotyy)
            sumtot = np.sum((hodnotyy - ymean)**2)
            Rsquared_lin = 1 - (residual/sumtot)
    
            if Rsquared_lin > Rsquared_threshold_linear:
    
                xn = np.array([xx for xx in range (min(hodnotyx),max(hodnotyx))])             #hodnoty indexu x
                yn = np.polyval(first_fit, xn).astype(int)                                      #hodnoty indexu y
    
                for l in range(0, len(yn)):
                    yn[l] = yn[l] - 90
    
                    if yn[l] >= 360:
    
                        yn[l] = yn[l] - 360
    
                    if yn[l] < 0:
    
                        yn[l] = yn[l] + 360
                
                xn = y[xn,0]                 #tady prevadim indexy na lat/lon aby to šlo vykreslit
                yn = x[0,yn]
    
                for i in range(0, len(yn)):
                    if yn[i] > 180:
                        yn[i] = 180

                    if yn[i] < -180:
                        yn[i] = -180

                crosspoint = 0

                combo = np.vstack((xn, yn))
    
                fronts.update({"front"+str(number_of_fronts): combo})
                number_of_fronts += 1
    
                typ_fronty = int(parametr[hodnotyx[1], hodnotyy[1]])
    
                smer_fronty = smer_front(typ_fronty,hodnotyx,hodnotyy,teplota,xn,yn)
                frontssmer.append(smer_fronty)
                frontstyp.append(typ_fronty)
    
                #print(typ_fronty, ":", smer_fronty)

                #vykreslení na mapu
                for l in range(0, len(yn)):
                    if yn[l] == -180. or yn[l] == 180.:
                        crosspoint = l
                        
                barva = "green"
    
                if (parametr[hodnotyx[0], hodnotyy[0]] == 1):
    
                    barva = "yellow"
    
                elif (parametr[hodnotyx[0], hodnotyy[0]] == -1):
    
                    barva = "purple"
    
                if crosspoint != 0:
    
                    yn1 = yn[:crosspoint]
                    xn1 = xn[:crosspoint]
                    yn2 = yn[crosspoint:]
                    xn2 = xn[crosspoint:]
    
                    m.plot(yn1, xn1, color = barva)
                    m.plot(yn2, xn2, color = barva)
    
                else:
    
                    m.plot(yn, xn, color = barva)
    
    return fronts, frontssmer, frontal_points, frontstyp

    
    #zakreslení křivky fronty
def functionF(params, x, y):
    
        a = params[0]
        b = params[1]
        c = params[2]
        alpha = params[3]
    
        return a*np.power((np.cos(alpha) * x + np.sin(alpha) * y), 2) + b * (np.cos(alpha) * x + np.sin(alpha) * y) + c - (-np.sin(alpha) * x + np.cos(alpha) * y)

def smer_front(typ_fronty,hodnotyx,hodnotyy,teplota,xn,yn):

    xysize = len(xn) - 1

    xdir = xn[xysize] - xn[0]
    ydir = yn[xysize] - yn[0]

    if ydir > 180:
        ydir = - ydir

    if xdir >= 0 and ydir >= 0:

        teplotni_rozdil = teplota[hodnotyx[int(xysize/2)] - 1, hodnotyy[int(xysize/2)] + 1] - teplota[hodnotyx[int(xysize/2)] + 1, hodnotyy[int(xysize/2)] - 1]

    elif xdir >= 0 and ydir <= 0:

        teplotni_rozdil = teplota[hodnotyx[int(xysize/2)] + 1, hodnotyy[int(xysize/2)] + 1] - teplota[hodnotyx[int(xysize/2)] - 1, hodnotyy[int(xysize/2)] - 1]

    elif xdir <= 0 and ydir >= 0:

        teplotni_rozdil = teplota[hodnotyx[int(xysize/2)] - 1, hodnotyy[int(xysize/2)] - 1] - teplota[hodnotyx[int(xysize/2)] + 1, hodnotyy[int(xysize/2)] + 1]

    elif xdir <= 0 and ydir <= 0:

        teplotni_rozdil = teplota[hodnotyx[int(xysize/2)] + 1, hodnotyy[int(xysize/2)] - 1] - teplota[hodnotyx[int(xysize/2)] - 1, hodnotyy[int(xysize/2)] + 1]

    else:

        print("ERROR: špatný teplotní rozdíl při určení druhu fronty")

    smer = 0

    if teplotni_rozdil * typ_fronty < 0:

        smer = 1

    elif teplotni_rozdil * typ_fronty > 0:

        smer = -1

    else:

        print("ERROR: špatné určení směru fronty")

    return smer

def najdi_tlak_min(frontal_points,p,m,y,x):
    for fronta in frontal_points:
        print(frontal_points[fronta][0][int(len(frontal_points[fronta][0])/2)]+90)
        print(frontal_points[fronta][1][int(len(frontal_points[fronta][0])/2)]+180)
        #xn = y[frontal_points[fronta][0][int(len(frontal_points[fronta][0])/2)]+90,0]    #tady prevadim indexy na lat/lon aby to šlo vykreslit
        #yn = x[0,frontal_points[fronta][1][int(len(frontal_points[fronta][0])/2)]+180]
        xn = frontal_points[fronta][0][int(len(frontal_points[fronta][0])/2)]+90    #tady prevadim indexy na lat/lon aby to šlo vykreslit
        yn = frontal_points[fronta][1][int(len(frontal_points[fronta][0])/2)]+180
        sousedi_rozdil=1
        while (sousedi_rozdil > 0):
            tlak_zde = p[xn,yn]
            if (yn != 0):
                tlak_left = p[xn,yn-1]
                indexx=xn
                indexy=yn-1
                sousedi_rozdil= tlak_zde-tlak_left
            else: 
                tlak_left = p[xn,359]
                indexx=xn
                indexy=359
                sousedi_rozdil= tlak_zde-tlak_left
            
            if (yn != 0):
                tlak_left = p[xn+1,yn-1]
                sousedi_mezirozdil= tlak_zde-tlak_left
                if(sousedi_mezirozdil>sousedi_rozdil):
                    indexx=xn+1
                    indexy=yn-1
                    sousedi_rozdil= tlak_zde-tlak_left
            else:
                tlak_left = p [xn+1,359]
                sousedi_mezirozdil= tlak_zde-tlak_left
                if(sousedi_mezirozdil>sousedi_rozdil):
                    indexx=xn+1
                    indexy=359
                    sousedi_rozdil= tlak_zde-tlak_left

            if (yn != 0):
                tlak_left = p[xn-1,yn-1]
                sousedi_mezirozdil= tlak_zde-tlak_left
                if(sousedi_mezirozdil>sousedi_rozdil):
                    indexx=xn-1
                    indexy=yn-1
                    sousedi_rozdil= tlak_zde-tlak_left
            else:
                tlak_left = p [xn-1,359]
                sousedi_mezirozdil= tlak_zde-tlak_left
                if(sousedi_mezirozdil>sousedi_rozdil):
                    indexx=xn-1
                    indexy=359
                    sousedi_rozdil= tlak_zde-tlak_left



            if (yn != 359):
                tlak_right = p[xn+1,yn+1]
                sousedi_mezirozdil= tlak_zde-tlak_right
                if(sousedi_mezirozdil>sousedi_rozdil):
                    indexx=xn+1
                    indexy=yn+1
                    sousedi_rozdil= tlak_zde-tlak_right
            else:
                tlak_right = p [xn+1,0]
                sousedi_mezirozdil= tlak_zde-tlak_right
                if(sousedi_mezirozdil>sousedi_rozdil):
                    indexx=xn+1
                    indexy=0
                    sousedi_rozdil= tlak_zde-tlak_right

            if (yn != 359):
                tlak_right = p[xn-1,yn+1]
                sousedi_mezirozdil= tlak_zde-tlak_right
                if(sousedi_mezirozdil>sousedi_rozdil):
                    indexx=xn-1
                    indexy=yn+1
                    sousedi_rozdil= tlak_zde-tlak_right
            else:
                tlak_right = p [xn-1,0]
                sousedi_mezirozdil= tlak_zde-tlak_right
                if(sousedi_mezirozdil>sousedi_rozdil):
                    indexx=xn-1
                    indexy=0
                    sousedi_rozdil= tlak_zde-tlak_right

            
            if (yn != 359):
                tlak_right = p[xn,yn+1]
                sousedi_mezirozdil= tlak_zde-tlak_right
                if(sousedi_mezirozdil>sousedi_rozdil):
                    indexx=xn
                    indexy=yn+1
                    sousedi_rozdil= tlak_zde-tlak_right
            else:
                tlak_right = p [xn,0]
                sousedi_mezirozdil= tlak_zde-tlak_right
                if(sousedi_mezirozdil>sousedi_rozdil):
                    indexx=xn
                    indexy=0
                    sousedi_rozdil= tlak_zde-tlak_right
                

            tlak_down = p[xn-1,yn]
            
            sousedi_mezirozdil= tlak_zde-tlak_down
            if(sousedi_mezirozdil>sousedi_rozdil):
                indexx=xn-1
                indexy=yn
                sousedi_rozdil= tlak_zde-tlak_down
            
            
            tlak_up = p[xn+1,yn]
  
            sousedi_mezirozdil= tlak_zde-tlak_up
            if(sousedi_mezirozdil>sousedi_rozdil):
                indexx=xn+1
                indexy=yn
                sousedi_rozdil= tlak_zde-tlak_up
                
            xnp = y[indexx,0]    #tady prevadim indexy na lat/lon aby to šlo vykreslit
            ynp = x[0,indexy]
            m.plot(ynp, xnp,'ko', color = "green", markersize=1)
            xn = indexx
            yn = indexy
        m.plot(ynp, xnp,'ko', color = "pink", markersize=4)



          
                


if __name__ == "__main__":
    main()