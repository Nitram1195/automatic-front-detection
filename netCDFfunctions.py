from netCDF4 import Dataset
import numpy as np

def write_data(name,data,lats,lons):
    dataset = Dataset(name, 'w',  format='NETCDF4_CLASSIC') 
    #create dimensions
    dataset.createDimension('lev', 6) 
    dataset.createDimension('lat', len(lats))
    dataset.createDimension('lon', len(lons)) 
    dataset.createDimension('time', 1 )
    #create 1-d variables
    dataset.createVariable('time', np.int32, ('time',)) 
    dataset.createVariable('lev', np.int32 , ('lev',)) 
    dataset.createVariable('lat', np.float32,   ('lat',))
    dataset.createVariable('lon', np.float32,  ('lon',)) 
    # Create the actual 2-d variable
    dataset.createVariable('T-diagnotic', np.float32, ('lat','lon')) 
    
    dataset['T-diagnotic'][:] = data
    dataset["lat"][:]  = lats  # (-90,90)   theta
    dataset["lon"][:]  = lons 
    
    dataset.close()
        
    return()
