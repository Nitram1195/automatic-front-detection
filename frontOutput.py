#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json

def vypis(fronts, frontsmer, fronttyp,cas):
     """format vystupu: 'cislo fronty' 'smer fronty (1 -- vlevo, -1 -- vpravo )' 'typ fronty(1 -- tepla, -1 -- studena )' """
     i=1
     f = open(repr(cas)+".txt", "w")
     for fronta in fronts:
         f.write(repr(i)+" "+repr(frontsmer[i-1])+ " "+repr(fronttyp[i-1])+"\n")
         for j in range(len(fronts[fronta][0])):
             f.write(repr(fronts[fronta][0][j])+" "+repr(fronts[fronta][1][j])+"\n")
         i=i+1

def vypis_JSON(fronts, frontsmer, fronttyp,cas,tlakove_nize=[],tlakove_vyse=[]):
    """vytvori JSON soubory"""
    data = {}
    i=1
    for fronta in fronts:
        souradnice = []
        for j in range(len(fronts[fronta][0])):
            souradnice.append([float(fronts[fronta][0][j]),float(fronts[fronta][1][j])]) 
        data[i] = {
                'smer': frontsmer[i-1],
                'typ' : fronttyp[i-1],
                'body' : souradnice
                }
        i=i+1
    if(tlakove_nize!=[]):    
        data[i]= {
                'typ' : 'nize',
                'body' : tlakove_nize
                }
    if(tlakove_vyse!=[]):  
        data[i+1]= {
                'typ' : 'vyse',
                'body' : tlakove_vyse
                }
    with open(repr(cas)+".json", "w") as write_file:
        json.dump(data, write_file)
    