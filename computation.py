#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np

R = 6370000 #m 

def smooth(a,ns):
    """ smooth 2D array """
    a1 = a.shape[0]
    a2 = a.shape[1]
    b = a[:]
    for k in range(ns):
        print("iteration {}".format(k))
        for i in range(1,a1-1):
            for j in range(1,a2-1):
                b[i,j] = 0.5*a[i,j] + 1/16.0*(a[i-1,j-1]+a[i,j-1]+a[i+1,j-1]+a[i+1,j]+a[i-1,j]+a[i-1,j+1]+a[i,j+1]+a[i+1,j+1])
    return(b)

def spherical_gradient(a, lats, lons, norm = False):
    """ return the gradient vector at each point in a, or , if norm = True, return the norm of the vector at each point. lats, lons in radians! """
    coords = np.meshgrid(lons,lats)
    # coords[0] = lon
    dlon = abs(lons[2] - lons[1])
    dlat = abs(lats[2] - lats[1])
    grd = np.gradient(a, dlat, dlon)
    g1 = np.array(grd[0]) / R  #thetova slozka
    g2 = np.array(grd[1]) / (R*np.sin(coords[1])) #phi slozka, coords[1] je uhel theta

    if norm == True:
        return(np.sqrt(g1**2 + g2**2))
    else:
        return(np.array([g1, g2]))

def spherical_hvorticity(u, v, lats, lons):
    """ return spherical voriticity """
    coords = np.meshgrid(lons,lats)
    # coords[0] = lon
    dlon = abs(lons[2] - lons[1])
    dlat = abs(lats[2] - lats[1])
    grd_u = np.gradient(u, dlat, dlon)[1] / (R*np.sin(coords[1]))
    grd_v = np.gradient(v, dlat, dlon)[0] / R

    hvort = grd_u - grd_v
    return(hvort)
    
def vorticity(u,v,lats,lons):
    """ return spherical voriticity """
    coords = np.meshgrid(lons,lats)
    dlon = abs(lons[2] - lons[1])
    dlat = abs(lats[2] - lats[1]) 
    
    grdv = np.gradient(v, dlat, dlon)
    grdusintheta = np.gradient(u*np.sin(coords[1]), dlat, dlon)
    
    vort = (grdv[0]-grdusintheta[1])/(R*np.sin(coords[1]))
    return(vort)
    
    
def coriolis_parameter(lats, lons):
    """ return coriolis parametr """
    coords = np.meshgrid(lons,lats)
    T=24*60*60
    omega=2*np.pi/T
    f = 2*omega*np.cos(coords[1])
    return(f)

def najdi_tlakove_nize(p,lats,lons):
    """ najde body ktere v okoli delta x a delta y 2 nemaji mensi hodnotu """
    minima = []
    for i in range(0,p.shape[0]):
        for j in range (0,p.shape[1]):
            jeToMinimum = True
            for xx in range(i-2,i+3):  #kontrolujeme jestli je to kolem ve vzdálenosti dvou políček není žadné menší
                for yy in range (j-2,j+3):
                    if(xx == -1): 
                        xx=1
                        if(yy<181):yy=yy+180
                        else: yy=yy-180
                    if(xx == -2): 
                        xx=1
                        if(yy<181):yy=yy+180
                        else: yy=yy-180
                    if(xx == 180): 
                        xx=179
                        if(yy<181):yy=yy+180
                        else: yy=yy-180
                    if(xx == 181): 
                        xx=178
                        if(yy<181):yy=yy+180
                        else: yy=yy-180
                    if(yy == -1): yy=359
                    if(yy == -2): yy=358
                    if(yy == 360): yy=0
                    if(yy == 361): yy=1
                    if(yy ==i and yy ==j):
                        continue
                    if (p[xx][yy] < p[i][j]):
                        jeToMinimum = False
            if(jeToMinimum==True and (110 < i  or i < 70)):
                minima.append([lats[i],lons[j]])
    return minima

def najdi_tlakove_vyse(p,lats,lons):
    """ najde body ktere v okoli delta x a delta y 2 nemaji vetsi hodnotu """
    maxima = []
    for i in range(1,p.shape[0]-1):
        for j in range (0,p.shape[1]):
            jeToMinimum = True
            for xx in range(i-2,i+3):  #kontrolujeme jestli je to kolem ve vzdálenosti dvou políček není žadné menší
                for yy in range (j-2,j+3):
                    if(xx == -1): 
                        xx=1
                        if(yy<181):yy=yy+180
                        else: yy=yy-180
                    if(xx == -2): 
                        xx=1
                        if(yy<181):yy=yy+180
                        else: yy=yy-180
                    if(xx == 180): 
                        xx=179
                        if(yy<181):yy=yy+180
                        else: yy=yy-180
                    if(xx == 181): 
                        xx=178
                        if(yy<181):yy=yy+180
                        else: yy=yy-180
                    if(yy == -1): yy=359
                    if(yy == -2): yy=358
                    if(yy == 360): yy=0
                    if(yy == 361): yy=1
                    if(yy ==i and yy ==j):
                        continue
                    if (p[xx][yy] > p[i][j]):
                        jeToMinimum = False
            if(jeToMinimum==True and (110 < i  or i < 70)):
                maxima.append([lats[i],lons[j]])
    return maxima


